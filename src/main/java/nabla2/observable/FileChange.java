package nabla2.observable;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;

import java.io.File;
import java.nio.file.*;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.*;

public class FileChange {
    public static Observable<WatchEvent<?>>
    of(File file) {
        Path dir = file.getParentFile().toPath();
        return Observable.create(subscriber -> {
           WatchService watcher = FileSystems.getDefault().newWatchService();
           try {
               dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
               watch(file.getCanonicalFile(), watcher, subscriber);
               subscriber.onComplete();
           }
           catch (Exception e) {
               subscriber.onError(e);
           }
        });
    }

    private static void
    watch(File target, WatchService watcher, ObservableEmitter<WatchEvent<?>> subscriber) {
        while (!subscriber.isDisposed()) {
            WatchKey watchKey;
            try {
                watchKey = watcher.take();
            }
            catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
            List<WatchEvent<?>> events = watchKey.pollEvents();
            Path targetDir = target.getParentFile().toPath();
            events.forEach((WatchEvent<?> ev) -> {
                if (ev.kind() == OVERFLOW) return;
                try {
                    File file = targetDir
                               .resolve((Path)ev.context())
                               .toFile();
                    if (!target.equals(file.getCanonicalFile())) return;
                    subscriber.onNext(ev);
                }
                catch (Exception e) {
                    subscriber.onError(e);
                }
            });
            watchKey.reset();
        }
    }
}
